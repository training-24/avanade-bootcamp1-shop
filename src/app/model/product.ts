export interface Product {
  id: string;
  name: string;
  cost: number;
  description: string;
  image: string;
  notes: string;
}
