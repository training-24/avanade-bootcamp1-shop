export interface Auth {
  name: string;
  token: string;
  role: 'admin' | 'guest';
}
