import { computed, Injectable, signal } from '@angular/core';
import { Product } from '../model/product';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  items = signal<Product[]>([])
  isEmpty = computed(() => !this.items().length)
  totalItems = computed(() => this.items().length)
  totalCosts = computed(() => this.items().reduce((acc, item) => acc + item.cost, 0))

  addToCart(product: Product) {
    this.items.update(items => {
      return [...items, product]
    })
  }

  removeFromCart(productToRemove: Product) {
    this.items.update(items => {
      return items.filter(item => item.id !== productToRemove.id)
    })
  }

  getTotalCostOLD() {
    let total = 0;
    this.items().forEach(item => {
      total += item.cost;
    })
    return total
  }

  getTotalCost() {
    return  this.items().reduce((acc, item) => acc + item.cost, 0)

  }
}
