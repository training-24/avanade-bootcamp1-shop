import { HttpClient, HttpParams } from '@angular/common/http';
import { computed, inject, Injectable, signal } from '@angular/core';
import { Router } from '@angular/router';
import { lastValueFrom, share } from 'rxjs';
import { Auth } from '../model/auth';

const baseUrl = 'http://localhost:3000'

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  http = inject(HttpClient)
  auth = signal<Auth | null>(null)

  isLogged = computed(() => !!this.auth())
  token = computed(() =>  this.auth()?.token)
  role = computed(() =>  this.auth()?.role)

  constructor() {
    const authFromLocalStorage = localStorage.getItem('auth')
    if (authFromLocalStorage) {
      this.auth.set(
        JSON.parse(authFromLocalStorage) as Auth
      )
    }
  }

  signIn(u: string, p: string) {
    const params = new HttpParams()
      .set('username', u)
      .set('password', p)

    const req$ =  this.http.get<Auth>(`${baseUrl}/login`, { params })
      .pipe(share())

    req$.subscribe(res => {
      this.auth.set(res)
      localStorage.setItem('auth', JSON.stringify(res))
    })
    return req$
  }


  signIn3(u: string, p: string): Promise<Auth> {
      const params = new HttpParams()
        .set('username', u)
        .set('password', p)

    return lastValueFrom(
      this.http.get<Auth>(`${baseUrl}/login`, { params })
    )
      .then(res => {
        this.auth.set(res)
        return res;
      })

  }

  signIn2(u: string, p: string): Promise<Auth> {
    return new Promise((resolve, reject) => {
      const params = new HttpParams()
        .set('username', u)
        .set('password', p)

      this.http.get<Auth>(`${baseUrl}/login`, { params })
        .subscribe(res => {
          this.auth.set(res)
          resolve(res)
          // return res;
        })
    })

  }

  logout() {
    this.auth.set(null)
    localStorage.removeItem('auth')
  }

}
