import { inject } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';

export const authGuard = () => {
  const authSrv = inject(AuthService)
  const router = inject(Router)
  if (!authSrv.isLogged()) {
    router.navigateByUrl('login')
  }
  return authSrv.isLogged();
}
