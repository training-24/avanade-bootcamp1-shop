import { HttpInterceptorFn } from '@angular/common/http';
import { inject } from '@angular/core';
import { Router } from '@angular/router';
import { catchError, of } from 'rxjs';
import { AuthService } from './auth.service';

export const authInterceptor: HttpInterceptorFn = (req, next) => {
  const authService = inject(AuthService)
  const tk =  authService.token();
  const router = inject(Router)

  if (tk && req.url.includes('localhost')) {
    const cloneReq =  req.clone({
      setHeaders: {
        Authorization: tk
      }
    })
    return next(cloneReq)
      .pipe(
        catchError((err) => {
          console.log(err)
          switch(err.status) {
            case 401:
            case 0:
              router.navigateByUrl('login')
              break;

            case 404:
              break;
          }
          return of(err)
        })
      )
  }

  return next(req)
    .pipe(
      catchError((err) => {
        return of(err)
      })
    )
};
