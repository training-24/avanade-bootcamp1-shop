import { JsonPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, inject, ViewChild } from '@angular/core';
import { FormControl, ReactiveFormsModule, ValidationErrors, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {
  concatMap,
  debounceTime, delay,
  distinctUntilChanged,
  exhaustMap,
  filter,
  fromEvent,
  map,
  mergeMap,
  switchMap
} from 'rxjs';
import { ajax } from 'rxjs/internal/ajax/ajax';
import { AuthService } from '../../core/auth.service';
import { Auth } from '../../model/auth';
import { PercErrorBarSignalsComponent } from '../../shared/perc-error-bar-signals.component';
import { PercErrorBarComponent } from '../../shared/perc-error-bar.component';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    JsonPipe,
    PercErrorBarComponent,
    PercErrorBarSignalsComponent
  ],
  template: `
    
    
    <div class="flex flex-col gap-3 w-96 mx-auto">
      <h1 class="page-title text-center">LOGIN</h1>
      
      @if(inputUserName.errors?.['required']) {
        <div class="text-red-500">required</div>
      }
      @if(inputUserName.errors?.['missingAt']) {
        <div class="text-red-500">
          Missing {{inputUserName.errors?.['minlength']['requiredLength'] - inputUserName.errors?.['minlength']['actualLength']}} chars
        </div>
      }
      
      <div>
        <input 
          type="text" class="input input-bordered w-full" placeholder="Username"
          #inputRef
          [formControl]="inputUserName"
        >
        <app-perc-error-bar [errors]="inputUserName.errors" />
        
      </div>
      
      
      <div>
        <input 
          type="text" class="input input-bordered w-full" placeholder="Password"
          [formControl]="inputPassword"
        >
        <perc-error-bar-signals [errors]="inputPassword.errors" />
       
      </div>
      
      <button 
        class="btn btn-primary"
        (click)="signIn()"
        [disabled]="inputUserName.invalid || inputPassword.invalid"
      >SIGNIN</button>
    </div>
  `,
  styles: ``
})
export default  class LoginComponent {
  @ViewChild('inputRef') inputRef!: ElementRef<HTMLInputElement>
  authSrv = inject(AuthService)
  router = inject(Router)
  http = inject(HttpClient)

  inputUserName = new FormControl('', {
    nonNullable: true,
    validators: [
      Validators.required,
      Validators.minLength(2)]
  })
  inputPassword = new FormControl('', {
    validators: [
      Validators.required,
      Validators.minLength(2)]
  })

  constructor() {


    /*this.inputUserName.valueChanges
      .pipe(
        filter(text => text!.length > 2),
        debounceTime(700),
      )
      .subscribe((text) => {
        console.log('hello:', text)
      })*/
  }

  signIn() {
    this.authSrv.signIn(
      this.inputUserName.value,
      this.inputPassword.value!
    )
      .subscribe((res, ) => {
        console.log(res.token)
        this.router.navigateByUrl('cms')
      })
  }


  ngAfterViewInit() {
    console.log(this.inputRef.nativeElement)

    ajax.getJSON('..')
      .pipe()
      .subscribe()

    fromEvent(this.inputRef.nativeElement, 'input')
      .pipe(
        map(ev => (ev.target as HTMLInputElement).value),
        exhaustMap((text) => this.http.get(`https://api.openweathermap.org/data/2.5/weather?q=${text}&units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`).pipe(delay(1000))),
      )
      .subscribe(meteo => {
        console.log(meteo)
      })
  }
}

