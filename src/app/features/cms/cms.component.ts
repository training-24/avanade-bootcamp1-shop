import { JsonPipe } from '@angular/common';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, inject, signal } from '@angular/core';
import { catchError, of } from 'rxjs';
import { Product } from '../../model/product';

@Component({
  selector: 'app-cms',
  standalone: true,
  imports: [
    JsonPipe
  ],
  template: `
    <p>
      cms works!
    </p>
    
    <pre>{{products() | json}}</pre>
  `,
  styles: ``
})
export default class CmsComponent {
  http = inject(HttpClient)
  products = signal<Product[] | null>([])

  constructor() {
    of(1, 2, 3, 4)
      .subscribe(val => {
        console.log(val)
      })

    // ----o-o-o-o-o--o-o---x-------o-o-o-o-o-o->
    this.http.get<Product[]>('http://localhost:3000/products')
      .subscribe({
        next: res => {
          console.log(res)
          this.products.set(res)
        },
        error: () => {
          window.alert('ciao')
        }
      })
  }
}
