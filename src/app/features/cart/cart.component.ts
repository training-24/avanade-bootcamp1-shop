import { Component, inject } from '@angular/core';
import { CartService } from '../../core/cart.service';

@Component({
  selector: 'app-cart',
  standalone: true,
  imports: [],
  template: `
   
    @for(item of cartService.items(); track item.id) {
      <li>
        {{item.name}} - {{item.cost}}
        
        <button class="btn" (click)="cartService.removeFromCart(item)">REMOVE</button>
      </li>
    } @empty {
      <div>Cart is Empty</div>
    }
  `,
  styles: ``
})
export default class CartComponent {

  cartService = inject(CartService)

}
