import { JsonPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, computed, inject, signal } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CartService } from '../../core/cart.service';
import { Product } from '../../model/product';
import { CardComponent } from '../../shared/card.component';
import { HeroComponent } from '../../shared/hero.component';
import { ProductCardComponent } from '../../shared/product-card.component';
import { ProductsListComponent } from './components/products-list.component';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [
    CardComponent,
    JsonPipe,
    ProductCardComponent,
    HeroComponent,
    ProductsListComponent,
  ],
  template: `

    <input
      (input)="search(input.value)"
      #input
      type="text" class="input input-accent">
    
    <app-hero title="My Shop"/>
    <app-products-list 
      [products]="products()"
      (addToCart)="cartService.addToCart($event)"
    />

    <pre>{{products() | json}}</pre>
    
  `,
  styles: ``,
})
export default  class HomeComponent {
  http = inject(HttpClient)
  cartService = inject(CartService)
  products = signal<Product[]>([])

  timeout: number = 0;

  constructor() {
    this.http.get<Product[]>('http://localhost:3000/products')
      .subscribe(res => {
        this.products.set(res);
      })
  }

  search(text: string) {
    clearTimeout(this.timeout)
    this.timeout = window.setTimeout(() => {
      this.http.get<Product[]>(`http://localhost:3000/products?q=${text}`)
        .subscribe(res => {
          this.products.set(res);
        })
    }, 1000)
  }
}
