import { NgIf } from '@angular/common';
import { Component, computed, EventEmitter, input, Input, Output, Signal } from '@angular/core';
import { Product } from '../../../model/product';
import { ProductCardComponent } from '../../../shared/product-card.component';

@Component({
  selector: 'app-products-list',
  standalone: true,
  template: `

    total: {{ total() }}
    <div class="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-y-6">
      @for(product of products(); track product.id) {
        <div class="flex justify-center">
          <app-product-card
            class="w-full"
            [product]="product"
            (addToCart)="addToCart.emit($event)"
          />
          
        </div>
      }
    </div>
    
  `,
  imports: [
    ProductCardComponent,
    NgIf
  ],
  styles: ``
})
export class ProductsListComponent {
  @Output() addToCart = new EventEmitter()

  products = input<Product[]>([])

  total = computed(() => {
    return this.products().length
  })

  doSomething(p: Product) {
    console.log(p)

  }
}
