import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { NavbarComponent } from './core/navbar.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, NavbarComponent],
  template: `
    <app-navbar />
   
    <div class="max-w-screen-lg  mx-6 lg:mx-auto">
    <router-outlet />
    </div>
  `,
  styles: [],
})
export class AppComponent {
  title = 'avanade-2024-shop';
}
