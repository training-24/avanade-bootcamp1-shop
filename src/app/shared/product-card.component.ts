import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  inject,
  input,
  Input,
  Output,
  Signal,
  signal
} from '@angular/core';
import { CartService } from '../core/cart.service';
import { Product } from '../model/product';

@Component({
  selector: 'app-product-card',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [],
  template: `
    <div 
      class="card w-full sm:w-72 md:w-80 bg-base-100 shadow-xl hover:opacity-80">
      <figure class="bg-white">
        <img [src]="product().image" [alt]="product().name" />
      </figure>
      <div class="card-body">
        <h2 class="card-title">
          <div class="flex justify-between  w-full">
            <div>{{ product().name }}</div>
            <div>€ {{ product().cost}}</div>
          </div>
        </h2>
        <p>{{product().description}}</p>
        <div class="card-actions justify-end">
          <button 
            class="btn btn-primary" 
            (click)="addToCart.emit(product())">Add to Cart</button>
        </div>
      </div>
    </div>
    
  `,
  styles: ``
})
export class ProductCardComponent {
  @Output() addToCart = new EventEmitter()
  // @Input({ required: true }) product!: any;
  product = input.required<Product>()

  render() {
    console.log('render card')
  }
}
