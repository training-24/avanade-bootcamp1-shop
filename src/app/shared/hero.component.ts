import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-hero',
  standalone: true,
  imports: [],
  template: `
    <div class="relative">
      <h1 
        class="z-20 text-7xl absolute top-5 left-5 text-black"
      >
        {{title}}
      </h1>
      
      <img
        class="rounded-3xl my-4 w-full"
        src="https://img.freepik.com/free-photo/cyber-monday-shopping-sales_23-2148688502.jpg?w=740&t=st=1706695632~exp=1706696232~hmac=7059e719ad647f319e53e1bf43fb7870c6153b84a9fd53ce03f251e493b109e0" alt="">
    </div>
  `,
  styles: ``
})
export class HeroComponent {
  @Input({ required: true}) title!: string;
}
