import { Component, Input } from '@angular/core';
import { ValidationErrors } from '@angular/forms';

@Component({
  selector: 'app-perc-error-bar',
  standalone: true,
  imports: [],
  template: `
    <div
      class="bg-red-500 h-2 rounded-xl transition-all duration-1000"
      [style.width.%]="getPerc()"
      [style.background-color]="getPercColor()"
    ></div>
  `,
})
export class PercErrorBarComponent {
  @Input() errors: ValidationErrors | null = null;

  getPerc() {
    const isMinLengthError = this.errors?.['minlength'];

    if (!isMinLengthError) return 0;

    const requiredLength = this.errors?.['minlength']['requiredLength'];
    const actualLength = this.errors?.['minlength']['actualLength'];
    const perc = (actualLength / requiredLength)*100
    return perc ;
  }

  getPercColor() {
    const perc = this.getPerc();
    if (perc < 33) {
      return 'red'
    } else if (perc < 66) {
      return 'orange'
    }

    return 'yellow'
  }
}
