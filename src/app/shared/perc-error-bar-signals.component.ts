import { Component, computed, input, Input } from '@angular/core';
import { ValidationErrors } from '@angular/forms';

@Component({
  selector: 'perc-error-bar-signals',
  standalone: true,
  imports: [],
  template: `
    <div
      class="bg-red-500 h-2 rounded-xl transition-all duration-1000"
      [style.width.%]="getPerc()"
      [style.background-color]="getPercColor()"
    ></div>
  `,
})
export class PercErrorBarSignalsComponent {
  errors = input<ValidationErrors | null>(null);

  getPerc = computed(() => {
    const err = this.errors();
    const minlength = err?.['minlength'];
    if (!minlength) return 0;
    const requiredLength = minlength['requiredLength'];
    const actualLength = minlength['actualLength'];
    return  (actualLength / requiredLength)*100
  })

  getPercColor = computed(() => {
    const perc = this.getPerc();
    if (perc < 33) {
      return 'red'
    } else if (perc < 66) {
      return 'orange'
    }

    return 'yellow'
  })
}
