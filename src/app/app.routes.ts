import { Routes } from '@angular/router';
import { authGuard } from './core/auth.guard';

export const routes: Routes = [
  { path: 'home', loadComponent: () => import('./features/home/home.component')},
  {
    path: 'cms',
    loadComponent: () => import('./features/cms/cms.component'),
    canActivate: [authGuard]
  },
  { path: 'contact', loadComponent: () => import('./features/contact/contact.component')},
  { path: 'cart', loadComponent: () => import('./features/cart/cart.component')},
  { path: 'login', loadComponent: () => import('./features/login/login.component')},
  { path: '', redirectTo: 'home', pathMatch: 'full'}
];
